package com.amihaescu.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.amihaescu.domain.Conference} entity.
 */
public class ConferenceDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer participants;

    @NotNull
    private ZonedDateTime date;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParticipants() {
        return participants;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConferenceDTO conferenceDTO = (ConferenceDTO) o;
        if (conferenceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), conferenceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConferenceDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", participants=" + getParticipants() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
